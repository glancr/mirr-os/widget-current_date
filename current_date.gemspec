require 'json'
$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "current_date/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "current_date"
  s.version     = CurrentDate::VERSION
  s.authors     = ["Tobias Grasse"]
  s.email       = ["tg@glancr.de"]
  s.homepage    = "https://glancr.de/modules/current_date"
  s.summary     = "Widget for mirr.OS that shows the current date."
  s.description = "Shows the current date."
  s.license     = "MIT"
  s.metadata    = { 'json' =>
                  {
                    type: 'widgets',
                    title: {
                      enGb: 'Date',
                      deDe: 'Datum',
                      frFr: 'Date',
                      esEs: 'Fecha',
                      plPl: 'Data',
                      koKr: '날짜'
                    },
                    description: {
                      enGb: s.description,
                      deDe: "Zeigt das aktuelle Datum.",
                      frFr: 'Affiche la date actuelle.',
                      esEs: 'Muestra la fecha actual.',
                      plPl: 'Pokazuje bieżącą datę.',
                      koKr: '현재 날짜를 표시합니다.'
                    },
                    sizes: [
                      {
                        w: 3,
                        h: 1
                      },
                      {
                        w: 4,
                        h: 1
                      },
                      {
                        w: 4,
                        h: 2
                      },
                      {
                        w: 12,
                        h: 4
                      },
                      {
                        w: 21,
                        h: 3
                      },
                      {
                        w: 21,
                        h: 12
                      }
                    ],
                    languages: [:enGb, :deDe, :frFr, :esEs, :plPl, :koKr],
                    group: nil
                  }.to_json
                }

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_development_dependency 'rails'
end
