module CurrentDate
  class Configuration < WidgetInstanceConfiguration
    attribute :weekday, :string, default: "short"
    attribute :year, :string, default: "numeric"
    attribute :month, :string, default: "2-digit"
    attribute :day, :string, default: "2-digit"

    validates :day, :year, inclusion: %w[numeric 2-digit]
    validates :weekday, inclusion: %w[narrow short long]
    validates :month, inclusion: %w[2-digit narrow short long]
  end
end
